package org.store.ecommercestore.repository;

import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.store.ecommercestore.model.CategoryEntity;
import org.store.ecommercestore.model.ProductEntity;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Testing ProductEntityRepository class
 * @author Muhammad Farhan Khalid
 * @version 1.0.2
 * @since 04/11/2020
 */
@DataJpaTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class ProductEntityRepositoryTest {

    /**
     * Represents the instance of ProductEntityRepository
     */
    @Autowired
    private ProductEntityRepository productEntityRepository;

    /**
     * Represents the instance of CategoryEntityRepository
     */
    @Autowired
    private CategoryEntityRepository categoryEntityRepository;

    /**
     * Represents the instance of ProductEntity
     */
    private static ProductEntity sampleProduct = new ProductEntity();

    /**
     * setting and saving Product data will be testing before all
     */
    @BeforeAll
    void init(){
        CategoryEntity categoryEntity = new CategoryEntity();
        categoryEntity.setCategoryName("test");
        categoryEntityRepository.save(categoryEntity);
        sampleProduct.setName("test product");
        sampleProduct.setDescription("test descp");
        sampleProduct.setCategory(categoryEntity);
        ProductEntity saved = productEntityRepository.save(sampleProduct);

    }

    /**
     * test to save items to the repository
     */
    @Test
    @Order(1)
    void passIfProductIsAdded(){
        productEntityRepository.save(sampleProduct);
        assertThat(productEntityRepository.findAll().size(), equalTo(17));
    }


    /**
     * test to update the specific item from the category find all
     */
    @Test
    @Order(2)
    void passIfProductIsUpdated(){
        ProductEntity productEntity = productEntityRepository.findById(1L).get();
        productEntity.setDescription("update desc");
        productEntityRepository.save(productEntity);
        assertThat(productEntityRepository.findById(1L).get().getDescription(), equalTo("update desc"));
    }

    /**
     * test to delete the item from the repository
     */
    @Test
    @Order(4)
    void passIfProductIsDeleted(){
        productEntityRepository.delete(sampleProduct);
        assertThat(productEntityRepository.findAll().size(), equalTo(16));
    }

    /**
     * test to check if we can find product by name
     */
    @Test
    @Order(3)
    void passIfProductIsFoundByNameContaining(){
        assertFalse(productEntityRepository.findByNameContaining("test").isEmpty());
    }


}
