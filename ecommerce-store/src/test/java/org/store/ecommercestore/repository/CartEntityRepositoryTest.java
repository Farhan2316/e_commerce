package org.store.ecommercestore.repository;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.store.ecommercestore.model.CartEntity;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

/**
 * Testing CartEntityRepository class
 * @author Muhammad Farhan Khalid
 * @version 1.0.2
 * @since 04/11/2020
 */
@DataJpaTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class CartEntityRepositoryTest {

    /**
     * Represents the instance of CartEntityRepository
     */
    @Autowired
    private CartEntityRepository cartEntityRepository;

    /**
     * Represents the instance of CartEntityRepository
     */
    private static CartEntity testCart = new CartEntity();

    /**
     * Set Cart Holder email
     */
    static {
        testCart.setCartHolderEmail("test_mail@test.com");
    }

    /**
     * setting and saving cart data will be testing before all
     */
    @BeforeAll
    void init(){
        CartEntity cartEntity = new CartEntity();
        cartEntity.setCartHolderEmail("sample@test.com");
        cartEntityRepository.save(cartEntity);
    }

    /**
     * test to find all saved items from the cart
     */
    @Test
    @DisplayName("TEST :: adding cart")
    void findByCartHolderEmail() {
        cartEntityRepository.save(testCart);
        assertThat(cartEntityRepository.findAll().size(), equalTo(3));
    }

    /**
     * test to delete the item from the cart
     */
    @Test
    @DisplayName("TEST :: deleting cart")
    void passIfCartIsDeleted(){
        cartEntityRepository.delete(testCart);
        assertThat(cartEntityRepository.findAll().size(), equalTo(2));
    }

    /**
     * test to update the specific item from the cart find by id
     */
    @Test
    @DisplayName("TEST :: updating cart")
    void passIfCartIsUpdated(){
        CartEntity cartEntity = cartEntityRepository.findById(1L).get();
        cartEntity.setCartHolderEmail("update_email@test.com");
        cartEntityRepository.save(cartEntity);
        assertThat(cartEntityRepository.findById(1L).get().getCartHolderEmail(), equalTo("update_email@test.com"));
    }



}