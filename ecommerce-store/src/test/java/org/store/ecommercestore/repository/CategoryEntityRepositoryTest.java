package org.store.ecommercestore.repository;


import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.store.ecommercestore.model.CategoryEntity;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

/**
 * Testing CartEntityRepository class
 * @author Muhammad Farhan Khalid
 * @version 1.0.2
 * @since 04/11/2020
 */
@DataJpaTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class CategoryEntityRepositoryTest {

    /**
     * Represents the instance of CategoryEntityRepository
     */
    @Autowired
    private CategoryEntityRepository categoryEntityRepository;

    /**
     * Represents the instance of ProductEntityRepository
     */
    @Autowired
    private ProductEntityRepository productEntityRepository;

    /**
     * setting and saving category data will be testing before all
     */
    @BeforeAll
    public void init(){
        CategoryEntity beforeCategory = new CategoryEntity();
        beforeCategory.setCategoryName("beforeCategory");
        categoryEntityRepository.save(beforeCategory);
        productEntityRepository.deleteAll();
    }

    /**
     * test to save items to the repository
     */
    @Test
    @DisplayName("TEST :: adding category")
    void shouldReturnTrueIfCategoryHasBeenAddedToDatabase(){
        CategoryEntity testCategory = new CategoryEntity();
        testCategory.setCategoryName("testCategory");
        assertNotEquals(null, categoryEntityRepository.save(testCategory));
    }

    /**
     * test to find the categories from the category
     */
    @Test
    @DisplayName("TEST :: get categories")
    void shouldReturnTrueIfCategoriesAmountIsEqualToOne(){
        List<CategoryEntity> categories = categoryEntityRepository.findAll();
        assertThat(categories.size(), equalTo(5));
    }

    /**
     * test to delete the item from the category
     */
    @Test
    @DisplayName("TEST :: deleting category")
    void shouldReturnTrueIfCategoryHasBeenDeleted(){
        categoryEntityRepository.deleteById(3L);
        assertThat(categoryEntityRepository.findAll().size(), equalTo(4));

    }

    /**
     * test to update the specific item from the category find by id
     */
    @Test
    @DisplayName("TEST :: update category")
    void shouldReturnTrueIfCategoryHasBeenUpdated(){
        CategoryEntity category = categoryEntityRepository.findById(2L).get();
        category.setCategoryName("updatedCategory");
        categoryEntityRepository.save(category);
        assertThat(categoryEntityRepository.findById(2L).get().getCategoryName(), equalTo("updatedCategory"));
    }




}
