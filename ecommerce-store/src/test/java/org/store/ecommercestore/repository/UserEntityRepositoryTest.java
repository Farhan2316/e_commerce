package org.store.ecommercestore.repository;

import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.store.ecommercestore.model.UserEntity;

import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Testing UserEntityRepository class
 * @author Muhammad Farhan Khalid
 * @version 1.0.2
 * @since 04/11/2020
 */
@DataJpaTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class UserEntityRepositoryTest {

    /**
     * Represents the instance of UserEntityRepository
     */
    @Autowired
    private UserEntityRepository userEntityRepository;

    /**
     * Represents the token for user creation
     */
    private static final String userToken = UUID.randomUUID().toString();

    /**
     * setting and saving User data will be testing before all
     */
    @BeforeAll
    @DisplayName(value = "Sample data initialization ():")
    void addSampleUserEntity(){
        UserEntity userEntity = new UserEntity();
        userEntity.setEmail("testEmail@domain.com");
        userEntity.setUsername("testUsername");
        userEntity.setPassword("test_password");
        userEntity.setRole("ROLE_USER");
        userEntity.setToken(userToken);
        userEntityRepository.save(userEntity);
    }

    /**
     * Delete the user for clean up
     */
    @AfterEach
    void cleanUp(){
        userEntityRepository.deleteAll();
    }


    /**
     * test to check if user exist by the email
     */
    @Test
    @DisplayName("findByEmail(): method testing")
    void shouldReturnUserByEmailIfExisting() {
        assertTrue(userEntityRepository.findByEmail("testEmail@domain.com").isPresent());
    }

    /**
     * Test to check the user by token
     */
    @Test
    @DisplayName("findByToken(): method testing")
    void shouldReturnUserByTokenIfExisting(){
        assertTrue(userEntityRepository.findByToken(userToken).isPresent());
    }

    /**
     * test to find the user by the name
     */
    @Test
    @DisplayName("findByUsername(): method testing")
    void shouldReturnUserByUsernameIfExisting(){
        assertTrue(userEntityRepository.findByUsername("testUsername").isPresent());
    }

    /**
     * Test to delete the user
     */
    @Test
    @DisplayName("delete(): method testing")
    void shouldDeleteSelectedUser () {
        Optional<UserEntity> deleteUserObject = userEntityRepository.findByUsername("testUsername");
        assertTrue(deleteUserObject.isPresent());
        System.out.println(userEntityRepository.findAll().size());
        userEntityRepository.delete(deleteUserObject.get());
        assertEquals(2, userEntityRepository.findAll().size());
    }
}