package org.store.ecommercestore.exceptions;

/**
 * Rest-Controller, contains ADMIN authenticated endpoints
 * @author Muhammad Farhan Khalid
 * @version 1.0.2
 * @since 28/10/2020
 */

public class CustomException extends Exception {

    /**
     * Custome created Excepton
     * @param message message
     */
    public CustomException(String message) {
        super(message);
    }
}
