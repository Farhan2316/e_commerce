package org.store.ecommercestore.repository;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import org.store.ecommercestore.model.ProductEntity;

import java.util.List;

/**
 * Extension of ProductEntityRepository to provide additional methods to retrieve entities using the AbstractRepository
 * @author Muhammad Farhan Khalid
 * @version 1.0.2
 * @since 07/10/2020
 */

@Repository
public interface ProductEntityRepository extends AbstractRepository<ProductEntity, Long> {

    /**
     * Returns all ProductEntity  by the given options
     * @return all ProductEntity  by the given options
     */
    List<ProductEntity> findAll();

    /**
     * Retrieves an entity by its product name
     * @param name name
     * @return all the entities related to product name
     */
    List<ProductEntity> findByNameContaining(String name);

    /**
     * Returns a Page of entities meeting the paging restriction provided in the Pageable object and name
     * @param name name
     * @param pageable pageable
     * @return a page of order entities with respect to product name
     */
    Page<ProductEntity> findByNameContaining(String name, Pageable pageable);

    /**
     * Returns a Page of entities meeting the paging restriction provided in the Pageable object and categoryName
     * @param categoryName categoryName
     * @param pageable pageable
     * @return a page of order entities with respect to product name
     */
    Page<ProductEntity> findByCategoryCategoryName(String categoryName, Pageable pageable);

    /**
     * Returns a Page of entities meeting the paging restriction provided in the Pageable object
     * @param pageable pageable
     * @return a page of entities
     */
    Page<ProductEntity> findAll(Pageable pageable);

}
