package org.store.ecommercestore.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.store.ecommercestore.model.OrderEntity;
import org.store.ecommercestore.model.UserEntity;

import java.util.List;

/**
 * Extension of OrderRepository to provide additional methods to retrieve entities using the AbstractRepository
 * @author Muhammad Farhan Khalid
 * @version 1.0.2
 * @since 07/10/2020
 */

@Repository
public interface OrderRepository extends AbstractRepository<OrderEntity, Long> {

    /**
     * Retrieves an entity by its Order Holder
     * @param userEntity userEntity
     * @return all the entities related to Order holder
     */
    List<OrderEntity> findByOrderHolder(UserEntity userEntity);

    /**
     * Returns all OrderEntity  by the given options
     * @return all OrderEntity  by the given options
     */
    List<OrderEntity> findAll();

    /**
     * Returns a Page of entities meeting the paging restriction provided in the Pageable object and username
     * @param username username
     * @param pageable pageable
     * @return a page of order entities with respect to categories
     */
    Page<OrderEntity> findByOrderHolderUsername(String username, Pageable pageable);
}
