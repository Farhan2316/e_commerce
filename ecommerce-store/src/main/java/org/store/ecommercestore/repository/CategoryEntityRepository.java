package org.store.ecommercestore.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.store.ecommercestore.model.CategoryEntity;

import java.util.List;

/**
 * Extension of CategoryEntityRepository to provide additional methods to retrieve entities using the AbstractRepository
 * @author Muhammad Farhan Khalid
 * @version 1.0.2
 * @since 07/10/2020
 */

@Repository
public interface CategoryEntityRepository extends AbstractRepository<CategoryEntity, Long> {

    /**
     * Returns all CategoryEntity  by the given options
     * @return all CategoryEntity  by the given options
     */
    List<CategoryEntity> findAll();

    /**
     * Returns a Page of entities meeting the paging restriction provided in the Pageable object
     * @param pageable pageable
     * @return a page of entities
     */
    Page<CategoryEntity> findAll(Pageable pageable);

    /**
     * Returns a Page of entities meeting the paging restriction provided in the Pageable object and categories name
     * @param categoryName categoryName
     * @param pageable pageable
     * @return a page of entities with respect to categories
     */
    Page<CategoryEntity> findByCategoryNameContaining(String categoryName, Pageable pageable);

}
