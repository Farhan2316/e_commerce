package org.store.ecommercestore.repository;

import org.springframework.stereotype.Repository;
import org.store.ecommercestore.model.ForgotPasswordToken;

import java.util.Optional;

/**
 * Extension of CategoryEntityRepository to provide additional methods to retrieve entities using the AbstractRepository
 * @author Muhammad Farhan Khalid
 * @version 1.0.2
 * @since 07/10/2020
 */

@Repository
public interface ForgotPasswordRepository extends AbstractRepository<ForgotPasswordToken, Long>{

    /**
     * Retrieves an entity by its token
     * @param token token
     * @return the entity if available otherwise return null
     */
    Optional<ForgotPasswordToken> findByForgotPasswordToken(String token);
}
