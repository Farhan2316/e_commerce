package org.store.ecommercestore.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.store.ecommercestore.model.UserEntity;

import java.util.List;
import java.util.Optional;

@Repository
@Component
public interface UserEntityRepository extends AbstractRepository<UserEntity, Long> {

    /**
     * Retrieves an entity by its username
     * @param username username
     * @return the entity if available otherwise return null
     */
    Optional<UserEntity> findByUsername(String username);

    /**
     * Retrieves an entity by its email
     * @param email email
     * @return the entity if available otherwise return null
     */
    Optional<UserEntity> findByEmail(String email);

    /**
     * Retrieves an entity by its token
     * @param token token
     * @return the entity if available otherwise return null
     */
    Optional<UserEntity> findByToken(String token);

    /**
     * Returns all UserEntity  by the given options
     * @return all CategoryEntity  by the given options
     */
    List<UserEntity> findAll();



    /**
     * Returns a Page of entities meeting the paging restriction provided in the Pageable object
     * @param pageable pageable
     * @return a page of entities
     */
    Page<UserEntity> findAll(Pageable pageable);

    /**
     * Returns a Page of entities meeting the paging restriction provided in the Pageable object and email
     * @param email email
     * @param pageable pageable
     * @return a page of order entities with respect to categories
     */
    Page<UserEntity> findByEmailContaining(String email, Pageable pageable);
}
