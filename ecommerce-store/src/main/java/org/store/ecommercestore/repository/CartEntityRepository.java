package org.store.ecommercestore.repository;


import org.springframework.stereotype.Repository;
import org.store.ecommercestore.model.CartEntity;


import java.util.Optional;

/**
 * Extension of CartEntityRepository to provide additional methods to retrieve entities using the AbstractRepository
 * @author Muhammad Farhan Khalid
 * @version 1.0.2
 * @since 07/10/2020
 */

@Repository
public interface CartEntityRepository extends AbstractRepository<CartEntity, Long> {

    /**
     * Retrieves an entity by its email
     * @param email email provided
     * @return the entity if available otherwise return null
     */
    Optional<CartEntity> findByCartHolderEmail(String email);
}
