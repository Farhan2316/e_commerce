package org.store.ecommercestore.repository;


import org.springframework.data.repository.PagingAndSortingRepository;
import org.store.ecommercestore.model.AbstractEntity;

import java.util.List;

/**
 * Extension of AbstractRepository to provide additional methods to retrieve entities using the pagination and sorting abstraction
 * @author Muhammad Farhan Khalid
 * @version 1.0.2
 * @since 07/10/2020
 */
public interface AbstractRepository<T extends AbstractEntity, ID> extends PagingAndSortingRepository<T, ID>  {
        /**
         * Returns all instances of the type
         * @return all entities by the given options
         */
        List<T> findAll();
}





