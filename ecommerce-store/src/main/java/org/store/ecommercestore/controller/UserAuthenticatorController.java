package org.store.ecommercestore.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;
import org.store.ecommercestore.mapper.*;
import org.store.ecommercestore.model.UserEntity;
import org.store.ecommercestore.service.AuthenticationService;
import org.store.ecommercestore.service.EmailService;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Rest-Controller, contains User class end points
 * @author Muhammad Farhan Khalid
 * @version 1.0.2
 * @since 25/10/2020
 */
@RestController
@RequestMapping("/user")
@CrossOrigin("localhost:4200")
public class UserAuthenticatorController {

    /**
     * Represents the instance of EmailService
     */
    @Autowired
    private EmailService emailService;

    /**
     * Represents the instance of AuthenticationService
     */
    @Autowired
    private AuthenticationService authenticationService;

    /**
     * API to create new user account
     * @param userEntity userEntity
     * @param bindingResult bindingResult
     * @return the https response
     */
    @PostMapping("/sign-up")
    public Object signUp(@RequestBody @Valid UserEntity userEntity, BindingResult bindingResult){

        if(bindingResult.hasErrors()){
            List<String> errors = bindingResult.getAllErrors().stream().map(ObjectError::getDefaultMessage)
                    .collect(Collectors.toList());
            return new ResponseEntity<>(new Response(HttpStatus.BAD_REQUEST, false, errors), HttpStatus.BAD_REQUEST);
        }

        String token = UUID.randomUUID().toString(); userEntity.setToken(token);

        if(authenticationService.signUp(userEntity)){
            emailService.sendActivationTokenMail(userEntity.getEmail(), token);
            return ResponseEntity.ok(new Response("User has been signed-up", HttpStatus.OK, true));
        }

        return new ResponseEntity<>("Try other credentials, username or email probably is taken", HttpStatus.CONFLICT);
    }

    /**
     * API to create an account with facebook
     * @param facebookUserData facebookUserData
     * @return the user with facebook
     */
    @PostMapping("/facebook-user")
    public Object getFacebookUserDetails(@RequestBody FacebookUserData facebookUserData){
        return authenticationService.facebookRegister(facebookUserData);
    }

    /**
     * API to create sign in
     * @return the sign in user
     */
    @GetMapping("/sign-in")
    public Object signIn(){
        return authenticationService.signIn();
    }

    /**
     *  API to create token
     * @param token token
     * @return the https status
     */
    @PostMapping("/token-activation")
    public Object activateUserAccount(@RequestBody ConfirmToken token){
        if(authenticationService.activateUserAccount(token.getToken())){
            return ResponseEntity.ok(new Response("User account activated", HttpStatus.OK, true));
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

    }

    /**
     * API to retrieve the password
     * @param emailObject emailObject
     * @param bindingResult bindingResult
     * @return the https response
     */
    @PostMapping("/forgot-password")
    public ResponseEntity<Object> forgotPassword(@RequestBody @Valid ForgotPasswordMapper emailObject, BindingResult bindingResult){
        if(bindingResult.hasErrors())
            return new ResponseEntity<>(new Response(HttpStatus.BAD_REQUEST, "This email is incorrect or empty"), HttpStatus.BAD_REQUEST);
        if(authenticationService.changePasswordLink(emailObject))
            return new ResponseEntity<>(new Response(HttpStatus.OK, "Forgot password link has been sent"), HttpStatus.OK);

        return new ResponseEntity<>("Something went wrong, maybe you provided mistaken email", HttpStatus.BAD_REQUEST);
    }

    /**
     * API to change the User password
     * @param changePasswordMapper changePasswordMapper
     * @return the https response
     */
    @PostMapping("/change-password")
    public Object changePassword(@RequestBody ChangePasswordMapper changePasswordMapper) {
        if(authenticationService.changePassword(changePasswordMapper.getToken(), changePasswordMapper.getPassword())){
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return ResponseEntity.badRequest();
    }


}
