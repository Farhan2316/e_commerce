package org.store.ecommercestore.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.store.ecommercestore.service.ShoppingService;
import org.store.ecommercestore.service.StripeService;

import java.util.Map;

/**
 * Rest-Controller, for payment
 * @author Muhammad Farhan Khalid
 * @version 1.0.2
 * @since 25/10/2020
 */
@RestController
@RequestMapping("/payment")
public class PaymentController {

    /**
     * Represents the instance of StripeService
     */
    @Autowired
    private StripeService stripeService;

    /**
     * Represents the instance of ShoppingService
     */
    @Autowired
    private ShoppingService shoppingService;

    /**
     * API to create the payment
     * @return checkout session
     */
    @GetMapping("/stripe/session-id")
    public Map<String, String> createStripPayment(){
        return stripeService.createCheckoutSession(shoppingService.showCurrentProductsInCart());
    }

}
