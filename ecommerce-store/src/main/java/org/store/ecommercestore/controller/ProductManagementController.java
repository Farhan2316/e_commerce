package org.store.ecommercestore.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.store.ecommercestore.model.CategoryEntity;
import org.store.ecommercestore.model.ProductEntity;
import org.store.ecommercestore.service.ShopManagementService;
import org.store.ecommercestore.service.ShoppingService;

import java.util.List;

/**
 * Rest-Controller, contains Product class end points
 * @author Muhammad Farhan Khalid
 * @version 1.0.2
 * @since 25/10/2020
 */
@RestController
@RequestMapping("/products")
public class ProductManagementController {

    /**
     * Represents the instance of ShoppingService
     */
    @Autowired
    private ShoppingService shoppingService;

    /**
     * Represents the instance of ShopManagementService
     */
    @Autowired
    private ShopManagementService shopManagementService;

    /**
     * API to get all the products
     * @param page page
     * @param size size
     * @return the products with respect to page size
     */
    @GetMapping("/all-products")
    public Page<ProductEntity> getAllProducts(@RequestParam("page") int page, @RequestParam("size") int size){
        return shoppingService.getAllProducts(page, size);
    }

    /**
     * API to get the products by category name
     * @param page page
     * @param size size
     * @param categoryName categoryName
     * @return the products by category name on specific page size
     */
    @GetMapping("/products-by-category")
    public Page<ProductEntity> getProductsByCategory(@RequestParam("page") int page, @RequestParam("size") int size, @RequestParam("categoryName") String categoryName){
        return shoppingService.getAllProductsByCategory(page, size, categoryName);
    }

    /**
     * API to get all the categories
     * @return all the categories
     */
    @GetMapping("/all-categories")
    public List<CategoryEntity> getAllCategories() {
        return shopManagementService.getCategories();
    }

    /**
     * API to get all the products by iD
     * @param id id
     * @return the products by id
     */
    @GetMapping("/product-by-id")
    public ProductEntity getProductById(@RequestParam("productId") Long id){
        return shopManagementService.getProductById(id);
    }


    /**
     *API to get the categories on specific page size
     * @param page page
     * @param size size
     * @return all categories with pagination
     */

    @GetMapping("/all-categories-pag")
    public Page<CategoryEntity> getAllCategoriesPagination(@RequestParam("page") int page,
                                                           @RequestParam("size") int size) {
        return shopManagementService.getPaginationCategories(page, size);
    }

    /**
     * API to get teh categories by name
     * @param name name
     * @param page page
     * @param size size
     * @return the categories by name
     */
    @GetMapping("/categories-by-name")
    public Page<CategoryEntity> getCategoriesByName(@RequestParam("categoryName") String name,@RequestParam("page") int page,
                                                    @RequestParam("size") int size){
        return shopManagementService.getCategoriesByName(name, page, size);
    }

    /**
     * API to get the products by name
     * @param productName productName
     * @param page page
     * @param size size
     * @return the products by name via pagination
     */
    @GetMapping("/products-by-name")
    public Page<ProductEntity> getProductsByName(@RequestParam("productName") String productName,
                                                 @RequestParam("page") int page,
                                                 @RequestParam("size") int size){
        return shopManagementService.findProductsByName(productName, page, size);
    }


}
