package org.store.ecommercestore.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;
import org.store.ecommercestore.mapper.ProductRequest;
import org.store.ecommercestore.mapper.Response;
import org.store.ecommercestore.model.CategoryEntity;
import org.store.ecommercestore.model.UserEntity;
import org.store.ecommercestore.service.ShopManagementService;
import org.store.ecommercestore.service.UserService;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


/**
 * Rest-Controller, contains ADMIN authenticated endpoints
 * @author Muhammad Farhan Khalid
 * @version 1.0.2
 * @since 25/10/2020
 */

@RestController
@RequestMapping("/admin-panel")
public class AdminController {


    /**
     * Represents the instance of ShopManagementService
     */
    @Autowired
    private ShopManagementService shopManagementService;

    /**
     * Represents the instance of UserService
     */
    @Autowired
    private UserService userService;

    /**
     * API to add new product
     * @param addProductRequest addProductRequest
     * @param bindingResult bindingResult
     * @return the https response
     */
    @PostMapping(value = "/add-product")
    public ResponseEntity<Object> addProductEndpoint(@RequestBody ProductRequest addProductRequest, BindingResult bindingResult){

        if(addProductRequest.getProductImage().getImageBase64().isEmpty() || !addProductRequest.getProductImage().getImageBase64().contains("image")){
            return new ResponseEntity<>(new Response("Product image is not present"
                    , HttpStatus.BAD_REQUEST, false)
                    , HttpStatus.BAD_REQUEST);
        }

        if(bindingResult.hasErrors()){
            List<String> errors = bindingResult.getAllErrors().stream().map(ObjectError::getDefaultMessage)
                    .collect(Collectors.toList());
            return new ResponseEntity<>(new Response(HttpStatus.BAD_REQUEST, false, errors), HttpStatus.BAD_REQUEST);
        }

        if(shopManagementService.addProduct(addProductRequest.getProductInfo(), addProductRequest.getProductImage())){
            return new ResponseEntity<>(HttpStatus.OK);
        }


        return new ResponseEntity<>("Could not add the product, something went wrong", HttpStatus.BAD_REQUEST);

    }

    /**
     * API to add new category
     * @param categoryEntity categoryEntity
     * @param bindingResult bindingResult
     * @return the https response
     */
    @PostMapping("/add-category")
    public ResponseEntity<Object> addCategoryEndpoint(@RequestBody @Valid CategoryEntity categoryEntity, BindingResult bindingResult){
        if(bindingResult.hasErrors()){
            return ResponseEntity.badRequest().build();
        }
        shopManagementService.addCategory(categoryEntity);

        return ResponseEntity.ok().build();
    }

    /**
     * API to get user by Id
     * @param id id
     * @return the specific user by Id
     */
    @GetMapping("/users/byId")
    public Object getUserById(@RequestParam("userId") Long id){
        Optional<UserEntity> userById = userService.getUserById(id);
        if(userById.isEmpty()){
            return ResponseEntity.badRequest();
        }
        return userById.get();
    }

    /**
     * API to delete the user
     * @param id id
     */
    @DeleteMapping("/users")
    public void deleteUserByIdEndpoint(@RequestParam("userId") Long id) {
        userService.deleteUserById(id);
    }

    /**
     * API to update the user
     * @param userEntity userEntity
     * @param bindingResult bindingResult
     * @return the https status
     */
    @PutMapping("/users")
    public ResponseEntity<Object> updateUserEndpoint(@RequestBody @Valid UserEntity userEntity, BindingResult bindingResult){
        if(bindingResult.hasErrors()) return ResponseEntity.badRequest().build();
        userService.updateUser(userEntity);

        return ResponseEntity.ok().build();
    }

    /**
     * API to delete the category by ID
     * @param id id
     */
    @DeleteMapping("/delete-category")
    public void deleteCategoryEndpoint(@RequestParam("categoryId") Long id){
        shopManagementService.deleteCategory(id);
    }

    /**
     * API to delete the product by ID
     * @param id id
     */
    @DeleteMapping("/delete-product")
    public void deleteProductEndpoint(@RequestParam("productId") Long id){
        shopManagementService.deleteProductById(id);
    }

    /**
     * API to update the product
     * @param editProductRequest editProductRequest
     * @return the https status
     */
    @PutMapping("/edit-product")
    public ResponseEntity<Object> editProductEndpoint(@RequestBody ProductRequest editProductRequest){
        if(shopManagementService.editProduct(editProductRequest))
            return new ResponseEntity<>(HttpStatus.OK);
        else
            return new ResponseEntity<>("Something went wrong while updating the product", HttpStatus.BAD_REQUEST);
    }

    /**
     * API to get all the user on page with size
     * @param page page
     * @param size size
     * @return the users on list according to the page size
     */
    @GetMapping("/users")
    public Page<UserEntity> getAllUsersEndpoint(@RequestParam("page") int page, @RequestParam("size") int size){
        return userService.getAllUsers(page, size);
    }

    /**
     * API to get all the user on page with size
     * @param email email
     * @param page page
     * @param size size
     * @return the users by email on specific page
     */
    @GetMapping("/users-by-email")
    public Page<UserEntity> getUsersByEmail(@RequestParam("email") String email,
                                            @RequestParam("page") int page,
                                            @RequestParam("size") int size){
        return userService.getUsersByEmail(email, page, size);
    }



}
