package org.store.ecommercestore.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.store.ecommercestore.model.OrderEntity;
import org.store.ecommercestore.repository.OrderRepository;
import org.store.ecommercestore.repository.UserEntityRepository;
import org.store.ecommercestore.service.EmailService;
import org.store.ecommercestore.service.ShopManagementService;
import org.store.ecommercestore.service.ShoppingService;

import javax.validation.Valid;
import java.util.Optional;

/**
 * Rest-Controller, contains order end points
 * @author Muhammad Farhan Khalid
 * @version 1.0.2
 * @since 25/10/2020
 */
@RestController
@RequestMapping("/order")
public class OrderController {


    /**
     * Represents the instance of ShoppingService
     */
    @Autowired
    private ShoppingService shoppingService;

    /**
     * Represents the instance of ShopManagementService
     */
    @Autowired
    private ShopManagementService shopManagementService;

    /**
     * Represents the instance of UserEntityRepository
     */
    @Autowired
    private UserEntityRepository userEntityRepository;

    /**
     * Represents the instance of OrderRepository
     */
    @Autowired
    private OrderRepository orderEntityRepository;

    /**
     * Represents the instance of EmailService
     */
    @Autowired
    private EmailService emailService;

    /**
     * API to create a new order
     * @param orderEntity orderEntity
     * @return the https status
     */
    @PostMapping("/create-order")
    public ResponseEntity<Object> createOrder(@RequestBody @Valid OrderEntity orderEntity){
        shoppingService.createOrder(orderEntity);
        emailService.sendOrderInfo(orderEntity.getBuyerEmail(), orderEntity.getOrderProducts(), shoppingService.getCurrentCartPrice().toString());
        return ResponseEntity.ok().build();
    }

    /**
     * API to get the all orders
     * @return the https status
     */
    @GetMapping("/user-orders")
    public Object getUserOrders(){
        return new ResponseEntity<>(shoppingService.getUserOrders(), HttpStatus.OK);
    }

    /**
     * API to get the users by ID
     * @param id id
     * @return the order by id
     */
    @GetMapping("/orders/byId")
    public Object getOrderById(@RequestParam("orderId") Long id){
        Optional<OrderEntity> byId = orderEntityRepository.findById(id);
        if(byId.isEmpty()){
            return ResponseEntity.badRequest().build();
        }

        return byId.get();
    }

    /**
     * API to update the order
     * @param orderEntity orderEntity
     * @return the update order
     */
    @PutMapping("/update-order")
    public OrderEntity updateOrder(@RequestBody OrderEntity orderEntity){
        return orderEntityRepository.save(orderEntity);
    }

    /**
     * API to delete the order
     * @param id id
     */
    @DeleteMapping("/delete-order")
    public void deleteOrder(@RequestParam("orderId") Long id){
        orderEntityRepository.deleteById(id);
    }


    /**
     * API to get the orders by username
     * @param username username
     * @param page page
     * @param size size
     * @return the user order on page specific size
     */

    @GetMapping("orders-by-username")
    public Page<OrderEntity> getOrdersByUsername(@RequestParam("username") String username,
                                                @RequestParam("page") int page,
                                                 @RequestParam("size") int size){
        return shopManagementService.getOrdersByUsername(username, page, size);
    }

    /**
     * API to get all the user's order
     * @param page page
     * @param size size
     * @return the user order
     */
    @GetMapping("/orders")
    public Page<OrderEntity> getAllOrders(@RequestParam("page") int page, @RequestParam("size") int size){
        return orderEntityRepository.findAll(PageRequest.of(page, size));
    }


}
