package org.store.ecommercestore.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.store.ecommercestore.model.ProductEntity;
import org.store.ecommercestore.service.ShoppingService;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Rest-Controller, contains Cart end points
 * @author Muhammad Farhan Khalid
 * @version 1.0.2
 * @since 25/10/2020
 */

@RestController
@RequestMapping("/cart")
public class CartController {


    /**
     * Represent the instance of the ShoppingService
     */
    @Autowired
    private ShoppingService shoppingService;

    /**
     * API to add products to the cart the user
     * @param id id
     */
    @GetMapping("/add-product")
    public void addProductToCart(@RequestParam("productId") Long id){
        shoppingService.addProductToCart(id);
    }

    /**
     * API to check all the products
     * @return the product list from cart
     */
    @GetMapping("/all-products")
    public Set<ProductEntity> getAllCurrentCartProducts(){
        return shoppingService.showCurrentProductsInCart();
    }

    /**
     * API to delete the cart product by Id
     * @param id id
     */
    @DeleteMapping("/delete-product")
    public void deleteProductFromCart(@RequestParam("productId") Long id){
        shoppingService.deleteProductFromCart(id);
    }

    /**
     * API to delete all the cart product
     */
    @DeleteMapping("/deleteCart-products")
    public void deleteAllCartProducts(){
        shoppingService.removeAllProductsFromCart();
    }

    /**
     * API to get the price of products added in the cart
     * @return return data
     */
    @GetMapping("/current-price")
    public Map<String, Double> getCurrentPrice(){
        HashMap<String, Double> data = new HashMap<>();
        data.put("cartPrice", shoppingService.getCurrentCartPrice().doubleValue());
        return data;
    }

    /**
     * API to products count
     * @return the data count
     */
    @GetMapping("/product-count")
    public Map<String, Integer> getProductCount(){
        HashMap<String, Integer> data = new HashMap<>();
        data.put("cartCount", shoppingService.showCurrentProductsInCart().size());
       return data;
    }

}
