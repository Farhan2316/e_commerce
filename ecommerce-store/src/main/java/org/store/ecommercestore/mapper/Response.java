package org.store.ecommercestore.mapper;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.http.HttpStatus;

import java.util.List;

/**
 * Represents a typed HTTP response, as returned by a handler function
 * @author Muhammad Farhan Khalid
 * @version 1.0.2
 * @since 04/10/2020
 */
@Getter
@Setter
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Response {

    /**
     * Represents the instance of HttpStatus
     */
    private HttpStatus httpStatus;

    /**
     * Represents the message
     */
    private String message;

    /**
     * Represents the status
     */
    private boolean status;

    /**
     * Represents the message
     */
    private String [] messages;

    /**
     * Represents the erros
     */
    private List<String> errors;


    /**
     * Constructor of class
     * @param httpStatus httpStatus
     * @param message message
     * @param errors errors
     */
    public Response(HttpStatus httpStatus, String message, List<String> errors) {
        this.httpStatus = httpStatus;
        this.message = message;
        this.errors = errors;
    }

    /**
     * Constructor of class
     * @param httpStatus httpStatus
     * @param message message
     */
    public Response(HttpStatus httpStatus, String message) {
        this.httpStatus = httpStatus;
        this.message = message;
    }

    /**
     * Constructor of class
     * @param message message
     * @param httpStatus httpStatus
     * @param status status
     */
    public Response(String message, HttpStatus httpStatus, boolean status) {
        this.httpStatus = httpStatus;
        this.message = message;
        this.status = status;
    }

    /**
     * Constructor of class
     * @param httpStatus httpStatus
     * @param status status
     * @param messages messages
     */
    public Response(HttpStatus httpStatus, boolean status, List<String> messages) {
        this.httpStatus = httpStatus;
        this.status = status;
        this.messages = messages.toArray(new String[0]);

    }
}
