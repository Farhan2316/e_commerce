package org.store.ecommercestore.mapper;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.store.ecommercestore.mapper.mapperhelper.ProductImage;
import org.store.ecommercestore.model.ProductEntity;

/**
 * Represents the Mapper class for Product
 * @author Muhammad Farhan Khalid
 * @version 1.0.2
 * @since 16/10/2020
 */
@Setter
@Getter
@NoArgsConstructor
public class ProductRequest {

        /**
         * Represents the instance of ProductImage
         */
        private ProductImage productImage;

        /**
         * Represents the instance of ProductEntity
         */
        private ProductEntity productInfo;
}
