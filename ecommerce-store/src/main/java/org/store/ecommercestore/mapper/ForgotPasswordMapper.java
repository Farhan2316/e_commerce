package org.store.ecommercestore.mapper;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

/**
 * Represents the Mapper class for forgot password
 * @author Muhammad Farhan Khalid
 * @version 1.0.2
 * @since 16/10/2020
 */
@Getter
@Setter
@NoArgsConstructor
public class ForgotPasswordMapper {

    /**
     * Represents the email
     */
    @NotBlank
    @Email
    private String email;

}
