package org.store.ecommercestore.mapper;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Represents the Mapper class for change password
 * @author Muhammad Farhan Khalid
 * @version 1.0.2
 * @since 16/10/2020
 */
@Getter
@Setter
@NoArgsConstructor
public class ChangePasswordMapper {

    /**
     * Represents the Token for for changing the password
     */
    private String token;

    /**
     * Represents the password
     */
    private String password;

}
