package org.store.ecommercestore.mapper;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Represents the Mapper class for Facebook login
 * @author Muhammad Farhan Khalid
 * @version 1.0.2
 * @since 16/10/2020
 */
@NoArgsConstructor
@Setter
@Getter
public class FacebookUserData {

    /**
     * Represents the facebook user email
     */
    private String email;

    /**
     * Represents the facebook user Id
     */
    private String id;

    /**
     * Represents the facebook user name
     */
    private String name;

}
