package org.store.ecommercestore.mapper.mapperhelper;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Represents the Product image class
 * @author Muhammad Farhan Khalid
 * @version 1.0.2
 * @since 16/10/2020
 */
@Getter
@Setter
@NoArgsConstructor
public class ProductImage {

    /**
     * Represents the product image
     */
    private String imageBase64;

    /**
     * Represents the file name of product
     */
    private String fileName;

}
