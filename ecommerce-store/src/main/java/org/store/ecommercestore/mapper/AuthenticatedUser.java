package org.store.ecommercestore.mapper;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Represents the Mapper user class
 * @author Muhammad Farhan Khalid
 * @version 1.0.2
 * @since 16/10/2020
 */
@Getter
@Setter
@NoArgsConstructor
public class AuthenticatedUser {

    /**
     * Represents the User email
     */
    private String email;

    /**
     * Represents the Username
     */
    private String username;

    /**
     * Represents the role if the user or admin
     */
    private String role;

    /**
     * Represents the if User authenticated or not
     */
    private boolean isAuth;


    /**
     * Constructor of the class
     * @param email email
     * @param username username
     * @param role role
     * @param isAuthenticated isAuthenticated
     */
    public AuthenticatedUser(String email,String username, String role, boolean isAuthenticated) {
        this.email = email;
        this.username = username;
        this.role = role;
        this.isAuth = isAuthenticated;
    }
}
