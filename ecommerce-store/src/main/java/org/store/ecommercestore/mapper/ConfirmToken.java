package org.store.ecommercestore.mapper;

import lombok.Getter;
import lombok.Setter;

/**
 * Represents the Mapper class for token confirmation
 * @author Muhammad Farhan Khalid
 * @version 1.0.2
 * @since 16/10/2020
 */
@Getter
@Setter
public class ConfirmToken {

    /**
     * Represents the token
     */
    private String token;
}
