package org.store.ecommercestore.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.store.ecommercestore.exceptions.SimpleExceptionHandler;

import java.util.Collections;

/**
 * Core class which loads user-specific data
 * @author Muhammad Farhan Khalid
 * @version 1.0.2
 * @since 30/10/2020
 */

@EnableWebSecurity
@Configuration
public class WebSecurity extends WebSecurityConfigurerAdapter {

    /**
     * Represents the instance of UserDetailsImplementation
     */
    @Autowired
    private UserDetailsImplementation userDetailsImplementation;

    /**
     * Represents the instance of SimpleExceptionHandler
     */
    @Autowired
    private SimpleExceptionHandler simpleExceptionHandler;

    /**
     * Method to configure authentication manager to be used by Spring Security.
     *
     * @param auth the auth
     * @throws Exception the exception
     */

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsImplementation)
                .passwordEncoder(bCrypt());
    }

    /**
     * H2 database authentication
     * @param web web
     * @throws Exception Exception
     */
    @Override
    public void configure(org.springframework.security.config.annotation.web.builders.WebSecurity web) throws Exception {
        web.ignoring().antMatchers("/h2-console")
            .antMatchers("/h2-console/**");
    }

    /**
     * Method to configure security params, such as filtering between public and private links.
     *
     * @param http the http
     * @throws Exception the exception
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable();
        http.headers().frameOptions().sameOrigin();
        http.exceptionHandling().accessDeniedHandler(simpleExceptionHandler);
        http.authorizeRequests()
                .antMatchers("/h2-console/**").permitAll()
                .antMatchers("/admin-panel/*").hasRole("ADMIN")
                .antMatchers("/products/*").permitAll()
                .antMatchers("/payment/stripe/session-id").permitAll()
                .antMatchers("/user/sign-up").permitAll()
                .antMatchers("/user/facebook-user").permitAll()
                .antMatchers("/user/forgot-password").permitAll()
                .antMatchers("/user/change-password").permitAll()
                .antMatchers("/user/token-activation").permitAll()
                .antMatchers("/payment/stripe/make-payment").permitAll()
                .antMatchers("/cart/*").hasRole("USER")
                .antMatchers("/order/create-order").hasRole("USER")
                .antMatchers("/order/user-orders").hasRole("USER")
                .antMatchers("/order/orders").hasRole("ADMIN")
                .antMatchers("/order/delete-order").hasRole("ADMIN")
                .antMatchers("/order/update-order").hasRole("ADMIN")
                .anyRequest().authenticated()
                .and()
                .formLogin().disable()
                .httpBasic();

        http.cors().configurationSource(corsConfigurationSource());


    }



    @Bean
    public CorsConfigurationSource corsConfigurationSource(){
        return httpServletRequest -> {
            CorsConfiguration corsConfiguration = new CorsConfiguration();
            corsConfiguration.addAllowedOrigin("http://localhost:4200");
            corsConfiguration.setAllowCredentials(true);
            corsConfiguration.setAllowedMethods(Collections.singletonList("*"));
            corsConfiguration.setAllowedHeaders(Collections.singletonList("*"));
            return corsConfiguration;
        };
    }

    /**
     * BCryptPasswordEncoder - Password Encoder.
     *
     * @return the b crypt password encoder
     */

    @Bean
    public PasswordEncoder bCrypt(){
        return new BCryptPasswordEncoder();
    }

    @Bean
    public AuthenticationManager authManager() throws Exception {
        return super.authenticationManager();
    }
}
