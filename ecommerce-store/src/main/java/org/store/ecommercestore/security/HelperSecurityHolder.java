package org.store.ecommercestore.security;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * Core class which loads user-specific data
 * @author Muhammad Farhan Khalid
 * @version 1.0.2
 * @since 30/10/2020
 */
public class HelperSecurityHolder {

    /**
     *Method to get authenticated user
     * @return the authenticated username
     */
    public static String getAuthenticatedUsername(){
       UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
       return  userDetails.getUsername();
    }

    /**
     *
     * @return true if the token has been authenticated and the AbstractSecurityInterceptor does not need to present the token to the AuthenticationManager again for re-authentication.
     */
    public static boolean isAuthenticated(){
        return  SecurityContextHolder.getContext().getAuthentication().isAuthenticated();
    }
}
