package org.store.ecommercestore.security;

import javassist.NotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;
import org.store.ecommercestore.model.UserEntity;
import org.store.ecommercestore.repository.UserEntityRepository;

import java.util.Optional;

/**
 * Core class which loads user-specific data
 * @author Muhammad Farhan Khalid
 * @version 1.0.2
 * @since 30/10/2020
 */
@Service
public class UserDetailsImplementation implements UserDetailsService {

    /**
     * Represents the instance of UserEntityRepository
     */
    private UserEntityRepository userEntityRepository;

    /**
     * Represents the logger
     */
    private static final Logger logger = LoggerFactory.getLogger(UserDetailsImplementation.class);

    /**
     * Constructor of the class
     * @param userEntityRepository userEntityRepository
     */
    public UserDetailsImplementation(UserEntityRepository userEntityRepository) {
        this.userEntityRepository = userEntityRepository;
    }

    /**
     * Locates the user based on the username
     * @param username the username identifying the user whose data is required
     * @return a fully populated user record
     */
    @Override
    public UserDetails loadUserByUsername(String username) {
        Optional<UserEntity> userEntity = userEntityRepository.findByUsername(username);

        if(userEntity.isPresent())
            return userEntity.get();
        else
            try {
                throw new NotFoundException("User with this username is not present");
            } catch (NotFoundException e) {
                logger.error(e.getMessage(), e.getCause());
                return null;
            }

    }
}
