package org.store.ecommercestore.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.store.ecommercestore.model.UserEntity;
import org.store.ecommercestore.repository.CartEntityRepository;
import org.store.ecommercestore.repository.UserEntityRepository;

import java.util.Optional;

/**
 * UserService.class - contains CRUD-operations on Users(UserEntity.class) which use UserEntityRepository class
 * @author Muhammad Farhan Khalid
 * @version 1.0.2
 * @since 12/10/2020
 */
@Service
public class UserService {

    /**
     * Represent the instance of UserEntityRepository
     */
    @Autowired
    private UserEntityRepository userEntityRepository;

    /**
     * Represent the instance of CartEntityRepository
     */
    @Autowired
    private CartEntityRepository cartEntityRepository;

    /**
     * This method is to delete the User by Id
     * @param id id
     */
    public void deleteUserById(Long id){
        Optional<UserEntity> byId = userEntityRepository.findById(id);
        byId.ifPresent(userObject -> {
            userEntityRepository.deleteById(id);
            cartEntityRepository.delete(userObject.getCartEntity());
        });


    }


    /**
     *
     * @param page page
     * @param size size
     * @return all the UserEntity within the given page size
     */
    public Page<UserEntity> getUsersByEmail(String email, int page, int size){
        return userEntityRepository.findByEmailContaining(email, PageRequest.of(page, size));
    }

    /**
     * Method to update the user
     * @param userEntity userEntity
     */
    public void updateUser(UserEntity userEntity){
        userEntityRepository.findById(userEntity.getId()).ifPresent(userObject -> userEntityRepository.save(userEntity));
    }

    /**
     *
     * @param id id
     * @return the user by given Id if available otherwise null
     */
    public Optional<UserEntity> getUserById(Long id){
        return userEntityRepository.findById(id);
    }

    /**
     *
     * @param page page
     * @param size size
     * @return all the users on given page size
     */
    public Page<UserEntity> getAllUsers(int page, int size){
        return userEntityRepository.findAll(PageRequest.of(page, size));
    }


}
