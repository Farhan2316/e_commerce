package org.store.ecommercestore.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

/**
 * Represents the cart class
 * @author Muhammad Farhan Khalid
 * @version 1.0.2
 * @since 04/10/2020
 */

@Getter
@Setter
@NoArgsConstructor
@Entity
public class CartEntity extends AbstractEntity<Long> {
    /**
     * Represents the Cart Holder Email
     */
    private String cartHolderEmail;

    /**
     * Represents all the products available in Cart for buying
     */
    @ManyToMany
    private Set<ProductEntity> products;

}
