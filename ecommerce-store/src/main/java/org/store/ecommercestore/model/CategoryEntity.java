package org.store.ecommercestore.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * Represents the Categories available
 * @author Muhammad Farhan Khalid
 * @version 1.0.2
 * @since 04/10/2020
 */

@Getter
@Setter
@NoArgsConstructor
@Entity
public class CategoryEntity extends AbstractEntity<Long> {

    /**
     * Represents the Categories Name available
     */
    @NotBlank(message = "Category name cannot be empty")
    @Column(unique = true)
    private String categoryName;

    /**
     * Represents the relation between product and category
     */
    @JsonIgnore
    @OneToMany(mappedBy = "category", cascade = CascadeType.PERSIST, orphanRemoval = true)
    private List<ProductEntity> product;

}
