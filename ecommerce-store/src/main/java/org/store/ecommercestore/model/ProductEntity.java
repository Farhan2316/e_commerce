package org.store.ecommercestore.model;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.math.BigDecimal;


/**
 * Represents the Product class
 * @author Muhammad Farhan Khalid
 * @version 1.0.2
 * @since 04/10/2020
 */
@Getter
@Setter
@NoArgsConstructor
@Entity
public class ProductEntity extends AbstractEntity<Long> {

    /**
     * Represents the Product's quantity
     */
    private Long unitsInStock;

    /**
     * Represents the Product's name that can not be empty
     */
    @NotBlank
    private String name;

    /**
     * Represents the Product's price
     */
    private BigDecimal price;

    /**
     * Represents the Product's full description
     */
    @NotBlank
    @Column(length = 10000)
    private String description;

    /**
     * Represents the Product's Image
     */
    private String imageUrl;

    /**
     * Represents if the product is ready for sale
     */
    private boolean active;

    /**
     * Represents the category id related to products
     */
    @ManyToOne
    @JoinColumn(name = "category_id", nullable = false)
    private CategoryEntity category;




}
