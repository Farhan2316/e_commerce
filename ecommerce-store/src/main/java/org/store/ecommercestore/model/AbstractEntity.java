package org.store.ecommercestore.model;



import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * Represents the ID's for all model classes
 * @author Muhammad Farhan Khalid
 * @version 1.0.2
 * @since 04/10/2020
 */

@Getter
@Setter
@MappedSuperclass
public abstract class AbstractEntity<ID extends Serializable>  {

    /**
     * Represents the Id that will auto generate
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private ID id;

    /**
     * Represents the version
     */
    @Version
    private  Integer version;

    /**
     * Represents the Time when Id is created
     */
    @CreationTimestamp
    private LocalDateTime created;

    /**
     * Represents the time when Id is created
     */
    @UpdateTimestamp
    private LocalDateTime updated;


}
