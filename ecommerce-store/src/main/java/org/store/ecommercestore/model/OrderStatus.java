package org.store.ecommercestore.model;

/**
 * Represents the order processing class
 * @author Muhammad Farhan Khalid
 * @version 1.0.2
 * @since 04/10/2020
 * {@link #PENDING}
 * {@link #ACCEPTED}
 * {@link #ON_THE_WAY}
 * {@link #DELIVERED}
 */


public enum OrderStatus {
    /**
     * Pending order
     */
    PENDING,

    /**
     * Accepted order
     */
    ACCEPTED,

    /**
     * Order on the way
     */
    ON_THE_WAY,

    /**
     * Product delivered
     */
    DELIVERED
}
