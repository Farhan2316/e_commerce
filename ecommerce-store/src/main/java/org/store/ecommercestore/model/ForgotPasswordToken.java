package org.store.ecommercestore.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import java.time.LocalDateTime;

/**
 * Represents the forgot Password class
 * @author Muhammad Farhan Khalid
 * @version 1.0.2
 * @since 04/10/2020
 */

@Entity
@Getter
@Setter
@NoArgsConstructor
public class ForgotPasswordToken extends AbstractEntity<Long>{

    /**
     * Represents the Email for users to retrieve the password
     */
    private String forgotPasswordEmail;

    /**
     * Represents the generated token to retrieve the password
     */
    private String forgotPasswordToken;

    /**
     * Represents the generated token validity
     */
    private LocalDateTime tokenExpiry = LocalDateTime.now().plusHours(24);

    /**
     * Constructor for ForgotPasswordToken class
     * @param forgotPasswordEmail of user
     * @param forgotPasswordToken to retrieve the password
     */
    public ForgotPasswordToken(String forgotPasswordEmail, String forgotPasswordToken) {
        this.forgotPasswordEmail = forgotPasswordEmail;
        this.forgotPasswordToken = forgotPasswordToken;
    }
}
