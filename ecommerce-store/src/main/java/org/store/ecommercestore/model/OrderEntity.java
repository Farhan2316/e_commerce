package org.store.ecommercestore.model;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * Represents the order class
 * @author Muhammad Farhan Khalid
 * @version 1.0.2
 * @since 04/10/2020
 */
@Getter
@Setter
@NoArgsConstructor
@Entity
public class OrderEntity extends AbstractEntity<Long> {

    /**
     * Represents the buyer first name
     */
    @NotBlank
    private String buyerFirstName;

    /**
     * Represents the buyer last name
     */
    @NotBlank
    private String buyerSecondName;

    /**
     * Represents the buyer email
     */
    @NotBlank
    private String buyerEmail;

    /**
     * Represents the buyer country
     */
    @NotBlank
    private String shippingCountry;

    /**
     * Represents the buyer Street number or name
     */
    @NotBlank
    private String shippingStreet;

    /**
     * Represents the buyer city
     */
    @NotBlank
    private String shippingCity;

    /**
     * Represents the buyer County/Province or Region
     */
    @NotBlank
    private String shippingRegion;

    /**
     * Represents the buyer zip code
     */
    @NotBlank
    private String shippingPostalCode;

    /**
     * Represents the buyer order status if it order has been
     * accepted
     * on the way
     * delivered
     */
    @Enumerated(value = EnumType.STRING)
    private OrderStatus orderStatus;

    /**
     * Represents the buyer all the products that have been ordered
     */
    @ManyToMany(fetch = FetchType.EAGER)
    private List<ProductEntity> orderProducts;

    /**
     * Represents the User Id
     * A Single user can order many products
     */
    @ManyToOne
    private UserEntity orderHolder;

}
