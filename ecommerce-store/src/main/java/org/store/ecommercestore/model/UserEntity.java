package org.store.ecommercestore.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Collection;
import java.util.Collections;

/**
 * Represents the User class
 * @author Muhammad Farhan Khalid
 * @version 1.0.2
 * @since 04/10/2020
 */

@Getter
@Setter
@NoArgsConstructor
@Entity
public class UserEntity extends AbstractEntity<Long> implements UserDetails {


    /**
     * Represents the User's email
     */
    @NotBlank(message = "Email can't be empty")
    @Email(message = "This email address is not valid")
    private String email;

    /**
     * Represents the User's username
     */
    @NotBlank(message = "Username can't be empty")
    private String username;

    /**
     * Represents the User's password
     */
    @NotBlank(message = "Username can't be empty")
    @Size(min = 6, message = "Password is to short, consider longer password. At least 6 characters")
    private String password;

    /**
     * Represents if the person is user or admin
     */
    private String role;

    /**
     * Represents the User's token for registration
     */
    private String token;

    private String platform = "THIS";

    /**
     * Represents the cart id
     */
    @OneToOne(cascade = CascadeType.REMOVE)
    private CartEntity cartEntity;

    /**
     * Represents if the User's account is active or not
     */
    private boolean isAccountEnabled = false;


    /**
     *Returns the authorities granted to the user. Cannot return null
     * @return the authorities, sorted by natural key (never null)
     */
    @Override
    @JsonIgnore
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Collections.singleton(new SimpleGrantedAuthority(role));
    }


    /**
     * Indicates whether the user's account is enabled.
     * @param accountEnabled true if the user is not Enable, false otherwise
     */
    public void setAccountEnabled(boolean accountEnabled) {
        this.isAccountEnabled = accountEnabled;
    }

    /**
     * Indicates whether the user's account has expired. An expired account cannot be authenticated.
     * @return true if the user's account is valid (ie non-expired), false if no longer valid
     * isEnabled
     */
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    /**
     * Indicates whether the user is locked or unlocked. A locked user cannot be authenticated
     * @return true if the user is not locked, false otherwise
     */
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    /**
     * Indicates whether the user's credentials (password) has expired. Expired credentials prevent authentication
     * @return true if the user's credentials are valid (ie non-expired), false if no longer valid
     */
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    /**
     * Indicates whether the user is enabled or disabled. A disabled user cannot be authenticated
     * @return true if the user is enabled, false otherwise
     */
    @Override
    public boolean isEnabled() {
        return isAccountEnabled;
    }
}
